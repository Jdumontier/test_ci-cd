FROM maven:3-jdk-11 as build
COPY src /project/src
COPY pom.xml /project
RUN mvn -f /project/pom.xml clean package


FROM adoptopenjdk/openjdk11:alpine
ARG JAR_FILE=target/test_ci-cd-0.0.1-SNAPSHOT.jar
COPY ${JAR_FILE} app.jar
ENTRYPOINT ["java","-jar","/app.jar"]
